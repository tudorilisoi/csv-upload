/* eslint no-console: 0 */

// const child_process = require('child_process');
const path = require('path');
const uuid = require('uuid');
const fs = require('fs');
const _ = require('lodash')
const express = require('express');
const bodyParser = require('body-parser')
const compression = require('compression')
const compressionmw = compression(
    {
        threshold: 0,
        filter: function (req, res) {
            // console.error('REQ', req.url);
            return true
        }
    }
)


const stringify = require('csv-stringify');
const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackConfig = require('./webpack.config.js');
const CONFIG = require('./app/lib/server/config');

const isDeveloping = process.env.NODE_ENV !== 'production';
const port = CONFIG.SERVER.PORT
const app = express();

const workerProvider = require('./app/lib/server/worker-provider')
const importWorker = require('./app/lib/server/import-worker')
const jobStats = require('./app/lib/server/job-stats')
const jobID = require('./app/lib/server/job-id')
const fastInsert = require('./app/lib/server/db/fast-insert')
const Stats = jobStats.Stats


//enable JSON req parsing
const jsonParser = bodyParser.json({
    type: 'application/json',
    extended: true,
    limit: '1mb',
});

//enable large upload handling
const formidableParser = require('./app/lib/server/formidable-middleware')({
    hash: 'md5',
    allowedUrl: '/api/upload',
    uploadDir: CONFIG.TMPDIR,
})

const dblib = require('./app/lib/server/db/db')

const dbP = dblib.connect().then(createIndexes)

//TODO return result

function createIndexes(db) {
    db.collection('leads').createIndex({'_IDJOB': -1})
    db.collection('jobs').createIndex(
        {
            'id': -1,
            'startedAt': -1,
            'uploadStartedAt': -1,
            'completedAt': -1,
            'erroredAt': -1,
        })
    return db
}

function saveStats(stats) {
    const job = _.merge({id: stats.id}, stats.s)
    return fastInsert.insertDocuments('jobs', [job], (err, result)=> {
        if (err) {
            console.error(err)
            stats.set('erroredAt', new Date().getTime())
            stats.set('errors.log', 'ERROR_SAVE_JOB_FAILED')
        }
    })
}


function getWorker(opts = {}) {
    const worker = workerProvider.getWorker(importWorker, function (w) {
        w.onmessage = function (event) {
            // console.log("Worker #%s said :", w.__id, event.data);
            opts.onmessage && opts.onmessage(event)
        };
        w.onerror = function (err) {
            console.log("Worker ERR : ", err);
        };
    })
    return worker
}


function sendJSON(res, data, code = 200) {
    res.status(code)
    res.set('Content-type', 'application/json')
    res.send(JSON.stringify(data));
    res.end()
}

//API server


function clearFinishedJobs() {
    const allStats = jobStats.getAll()
    _.map(allStats, (stat, id)=> {
        if (stat.s.status === 'COMPLETED' || stat.s.errors.log.length) {
            jobStats.remove(id)
        }
    })
}

app.post('/api/stats', compressionmw, jsonParser, function response(req, res) {
    const ret = {
        result: 'ERROR',
        rows: [],
    }

    dbP.then(db=> {
        var myCursor = db
            .collection('jobs')
            .find({})
            .toArray((err, rows)=> {

                if (err) {
                    sendJSON(res, ret)
                    return
                }

                ret.result = 'OK'
                ret.rows = rows
                sendJSON(res, ret)

            })
    });


    // return sendJSON(res, ret)

})


app.get('/api/download/:idjob', compressionmw, function response(req, res) {

    const idjob = +req.params.idjob
    const reqid = 'DOWN_' + uuid() + ' :: ' + idjob
    console.log('PARAMS', req.params);

    dbP.then(db=> {

        db.collection('jobs')
            .find({id: idjob})
            .toArray((err, rows)=> {

                if (err || !rows || !rows.length) {
                    if (err) {
                        res.status(500)
                    } else {
                        res.status(204) //no content
                    }
                    res.end()
                    return
                }

                const stat = rows[0]

                console.time(reqid)

                res.setHeader('Content-disposition', 'attachment; filename=processed--' + stat.filename);
                res.setHeader('Content-type', 'text/csv');

                const spawn = require('child_process').spawn;
                const ls = spawn('node', ['app/lib/server/db/download.js', idjob]);
                ls.stdout.pipe(res)
                ls.on('close', ()=> {
                    console.timeEnd(reqid)
                })

                ls.on('error', ()=> {
                    res.status(500)
                    res.end()
                })


            })
    }).catch(e=> {
        res.status(500)
        res.end()
    })


})


app.post('/api/database', jsonParser, function response(req, res) {


    const op = req.body.op
    const idjob = +(req.body.idjob || 0)

    const ret = {
        result: 'ERROR',
        message: 'Unknown op',
        op: op,
        idjob: idjob,
    }

    switch (op) {
        case 'deletejob':
            return dblib.connect().then(db=> {

                if (!idjob) {
                    throw new Error('MISSING idjob')
                }
                db.collection('leads').deleteMany({_IDJOB: idjob})
                db.collection('jobs').deleteMany({id: idjob})
                jobStats.remove(idjob)

                ret.result = 'OK'
                ret.message = 'Dropped'
                return sendJSON(res, ret)

            }).catch(e=> {
                console.log('deletejob err', e);
                return sendJSON(res, ret, 500)
            })

            break;
        case 'drop':
            clearFinishedJobs()

            dblib.connect().then(db=> {
                let _res = db.collection('leads').drop()
                _res = _res && db.collection('jobs').drop()

                createIndexes(db)

                if (_res) {
                    ret.result = 'OK'
                    ret.message = 'Dropped'
                }
                return sendJSON(res, ret)
            }).catch(e=> {
                console.log('drop err', e);
                return sendJSON(res, ret, 500)
            })
            break;

        case 'clearfinished':
            clearFinishedJobs()
            ret.result = 'OK'
            ret.message = 'Cleared'
            return sendJSON(res, ret)
            break;

        default:
            return sendJSON(res, ret, 400)
            break;
    }
})


app.post('/api/upload/status', jsonParser, function response(req, res) {
    res.set('Content-type', 'application/json')
    res.send(JSON.stringify({
            stats: jobStats.getAll(),
            now: new Date().getTime(),
        })
    );
    res.end()
})


app.post('/api/upload', formidableParser, function response(req, res) {

    if (
        !(req.files && req.files.upload) || !req.fields.uploadStartedAt
    ) {
        res.set('Content-type', 'application/json')
        res.send(JSON.stringify({
            result: 'ERROR',
            message: 'Missing upload file or uploadStartedAt',
        }));
        res.end()
        return

    }

    // console.log('files', req.files);
    const {path, size, name, hash} = req.files.upload
    const emailField = req.fields.emailField ? req.fields.emailField : null

    const idjob = jobID()

    //initialize stats
    const _stats = Stats(idjob, {
        bytesTotal: size,
        filename: name,
        uploadStartedAt: req.fields.uploadStartedAt,
        emailField,
    })

    function sendResponse() {

        res.set('Content-type', 'application/json')
        res.send(JSON.stringify({
            result: 'OK',
            idjob,
        }));
    }

    setTimeout(sendResponse, 16)

    const w = getWorker({
        onmessage: (ev)=> {

            const {data} = ev
            const stats = data.id ? Stats(data.id) : null
            switch (data.type) {
                case 'STATE_ERROR':
                    stats && stats.log()

                    saveStats(stats)

                    fs.unlink(path, _.noop)
                    workerProvider.destroyWorker(w)

                    break;
                case 'STATS':
                    const args = data.args
                    const updatedStatus = data.args[1]
                    //update stats
                    stats.set.apply(stats, args)
                    const s = stats.get()

                    //parse completed
                    if (
                        [
                            jobStats.STATUS.PARSE_COMPLETED,
                            jobStats.STATUS.COMPLETED,
                        ].includes(updatedStatus)
                    ) {
                        stats.log()
                        if (updatedStatus === jobStats.STATUS.COMPLETED) {

                            saveStats(stats)

                            const millis = s.completedAt - s.startedAt
                            console.log('**********');
                            console.log('');
                            console.log('');
                            console.log('COMPLETED IN %s s', (millis / 1000).toFixed(1));
                            console.log('');
                            console.log('');
                            console.log('**********');
                            fs.unlink(path, _.noop)
                            workerProvider.destroyWorker(w)
                        }
                    }

                    break;
                default:
                    break;
            }
        }
    })

    console.log('JOBS', Object.keys(jobStats.getAll()));

    w.postMessage({
        mode: 'STAGED_UPSERT', //or 'SPAWN'
        // mode:'SPAWN',
        type: 'FILE_UPLOADED',
        file: {path, size, name, hash},
        idjob,
    })


})


if (isDeveloping) {
    const compiler = webpack(webpackConfig);
    const middleware = webpackMiddleware(compiler, {
        publicPath: webpackConfig.output.publicPath,
        contentBase: 'src',
        stats: {
            colors: true,
            hash: false,
            timings: true,
            chunks: false,
            chunkModules: false,
            modules: false,
        }
    });

    app.use(middleware);
    app.use(webpackHotMiddleware(compiler));
    app.get('*', function response(req, res) {
        res.write(middleware.fileSystem.readFileSync(path.join(__dirname, 'dist/index.html')));
        res.end();
    });
} else {

    app.use(compressionmw)
    app.use(express.static(__dirname + '/dist'));

    app.get('*', function response(req, res) {
        res.sendFile(path.join(__dirname, 'dist/index.html'));
    });
}

app.listen(port, function onStart(err) {
    if (err) {
        console.log(err);
    }
    console.info("SERVER PID is %d", process.pid);
    console.info('==> Listening on port %s. Open up http://0.0.0.0:%s/ in your browser.', port, port);
});
