// import ajax from 'tiny-ajax'
import ajax from './tiny-ajax-modified'
import _ from 'lodash'
import Promise from 'bluebird'

export default function Request(o) {
    this.xhr = null
    const defaults = {
        onInit: _.noop,
        onSuccess: _.noop,
        onError: _.noop,
        onComplete: _.noop,
        onAbort: _.noop,
        onUploadProgress: _.noop,
    }
    const opts = {...defaults, ...o};
    this.opts = opts

    this.abort = function () {
        if (this.xhr) {
            this.xhr.abort()
        }
    }


    this.send = function (data, opts = {}) {
        return new Promise((resolve, reject)=> {
            this._send(data, opts, resolve, reject)
        })
    }

    this.sendJSON = function (data, opts = {}) {
        const o = _.merge(
            {},
            opts,
            {
                _json: true,
                contentType: 'application/json',
                method: 'POST',
            })
        const self = this
        const _data = JSON.stringify(data)
        return new Promise((resolve, reject)=> {
            self._send.call(self, _data, o, resolve, reject)
        })
    }


    this._send = function (data, opts = {}, resolve, reject) {
        const {
            uri,
            onInit,
            onSuccess,
            onError,
            onComplete,
            onProgress,
            onUploadProgress,
            ...rest
        } = _.merge({}, this.opts, opts)

        this.abort()

        onInit()

        const self = this

        const finalOpts = {

            ...rest,

            data: data,
            progress: onProgress,
            uploadprogress: onUploadProgress,
            complete: onComplete,

            success: resp=> {
                // console.log('AJAX OK', resp, self.xhr);
                self.xhr = null
                if (finalOpts._json) {

                    try {
                        const parsedResponse = JSON.parse(resp)
                        resolve(parsedResponse)
                        onSuccess(parsedResponse)
                    } catch (err) {
                        reject(err)
                        onError(err)
                    }
                } else {
                    resolve(resp)
                    onSuccess(resp)
                }
            },
            error: err=> {
                let e = err
                if (self.xhr.status === 0) {
                    //self is an abort
                    e = new Error('ABORT')
                } else {
                    e = new Error(self.xhr.status)
                }
                // console.log('AJAX ERROR', e.message);
                self.xhr = null
                reject(e)
                onError(e)
            },


        }

        self.xhr = ajax.send(uri, finalOpts)
    }

    if (opts.debounceMillis) {
        this._debouncedSend = _.debounce(this.send.bind(this), opts.debounceMillis)
        this.debouncedSend = data=> {
            this.abort()
            this._debouncedSend.cancel()
            this._debouncedSend(data)
        }
    }
}
