class SimpleCache {
    constructor(maxSize, callback, initial = []) {
        this.arr = initial.slice(0)
        this.maxSize = maxSize
        this.callback = callback
    }

    _check() {
        const {maxSize, arr, callback} = this
        const size = arr.length
        if (size >= maxSize) {
            const chunk = this.arr.splice(0, this.maxSize)
            callback(chunk)
        }
    }

    flush() {
        const {maxSize, arr, callback} = this
        const chunk = arr.splice(0)
        if (chunk.length) {
            callback(chunk)
        }
    }

    empty() {
        this.arr = []
    }

    push(items) {
        if (Array.isArray(items)) {
            this.arr = this.arr.concat(items.slice(0))
        } else {
            this.arr.push(items)
        }
        this._check()
    }
}

module.exports = SimpleCache