module.exports = function csvWorker() {

    // postMessage();

    const _ = require('lodash')
    const dblib = require(__dirname + '/app/lib/server/db/db')
    const lead = require(__dirname + '/app/lib/server/db/models/lead')
    const db = dblib.connect()

    const counters = {}

    const mapValues = function (row) {
        const ret = {}
        _.map(lead.csvFieldMap, (val, key)=> {
            ret[val] = row[key]
        })
        return ret
    }

    postMessage({type: 'STATE_IDLE'})

    this.onmessage = function (event) {
        // postMessage('Hi ' + event.data);
        // self.close();

        // console.log('WORKER MSG');

        postMessage({type: 'STATE_BUSY'})

        const {rows, jobID, hasMore, rowCount} = event.data
        console.log('CHUNK', jobID, rows.length, rowCount);

        let docs = []

        rows.forEach(row=> {
            const values = mapValues(row)
            const doc = new lead.model(values)
            docs.push(doc)

        })

        lead.model.insertMany(docs, {}, ()=> {
            // console.log('INSERTED %d', docs.length);
            postMessage({type: 'STATE_IDLE'})
            postMessage({
                type: 'STATS',
                jobID,
                processedRows: docs.length,
            })

            docs = null

        })

        if (hasMore === 'no') {
            process.nextTick(()=> {

                console.log('FINISHED JOB #%s', jobID);
            })
        } else {
            // parser.write(data)
            // process.nextTick(()=> {
            // })
        }


    };
}
