/* jshint esversion: 6, node: true, freeze: true, undef: true */
'use strict';

/**
 * adapted from https://github.com/adam17/express-formidable/blob/master/lib/middleware.js
 */

const formidable = require('formidable');

function parse(opts = {}) {

    const {allowedURL, allowedMethod} = opts
    console.log(allowedURL);

    return (req, res, next) => {

        if (allowedURL && req.url !== allowedURL) {
            next()
            return
        }
        if (allowedMethod && req.method.toLowerCase() !== allowedMethod.toLowerCase()) {
            next()
            return
        }

        const form = new formidable.IncomingForm();
        Object.assign(form, opts);

        form.parse(req, (err, fields, files) => {
            if (err) {
                next(err);
                return;
            }

            Object.assign(req, {fields, files});
            next();
        });

    };
}

module.exports = parse;
