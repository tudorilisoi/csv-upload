module.exports = function importWorker() {

    // postMessage();

    const fs = require('fs')
    const _ = require('lodash')
    const lead = require(__dirname + '/app/lib/server/db/models/lead')
    const fastInsert = require(__dirname + '/app/lib/server/db/fast-insert')
    const spawn = require('child_process').spawn;
    const SimpleCache = require(__dirname + '/app/lib/SimpleCache')

    const jobStats = require(__dirname + '/app/lib/server/job-stats')
    const Stats = jobStats.Stats

    const CONFIG = require(__dirname + '/app/lib/server/config');

    var parse = require('csv-parse');

    /**
     * map a CSV row onto mongoose model fields
     * @param row
     * @returns {{}}
     */
    function mapValues(row) {
        const ret = {}
        _.map(lead.csvFieldMap, (val, key)=> {
            ret[val] = row[key]
        })
        return ret
    }


    function bulkInsert(file, id) {

        const self = this
        let parser
        let readStream
        let failed = false //unrecoverable err

        let parsed = 0;
        let stored = 0; //stored or err'd out
        let bytes = 0; //bytes read

        postMessage({id, type: 'STATE_BUSY'})
        const uid = 'insert:' + id
        console.time(uid)


        function fail(err) {
            cache.empty()

            //cleanup
            readStream.close()
            readStream.unpipe(parser)
            parser.end()

            this.finishTask()

            //update stats
            postMessage({id, type: 'STATS', 'args': ['erroredAt', new Date().getTime()]})
            postMessage({id, type: 'STATS', 'args': ['errors.log', err.toString()]})

            postMessage({id, type: 'STATE_ERROR'})
        }

        const cache = new SimpleCache(
            CONFIG.DB.INSERT_CHUNK_SIZE,
            rows=> {

                postMessage({id, type: 'STATS', 'args': ['parsed', rows.length]})

                readStream.pause()

                fastInsert.insertDocuments('leads', rows, (err, result)=> {

                    if (err) {

                        if (failed) {
                            return
                        }
                        failed = true

                        postMessage({id, type: 'STATS', 'args': ['errors.inserted', rows.length]})
                        fail.call(self, err)

                        return
                    } else {
                        postMessage({id, type: 'STATS', 'args': ['inserted', result.opsCount]})
                    }

                    readStream.resume()
                    stored += rows.length

                    if (bytes && parsed === stored) {
                        //success

                        this.finishTask()

                        postMessage({id, type: 'STATS', 'args': ['completedAt', new Date().getTime()]})
                        postMessage({id, type: 'STATS', 'args': ['status', jobStats.STATUS.COMPLETED]})

                    }
                })
            })


        parser = parse({columns: true})
        parser.on('readable', (data)=> {
            let record = parser.read()

            if (record === null) {

                //end of stream
                postMessage({id, type: 'STATS', 'args': ['recordCount', parsed]})
                return
            }

            while (record) {
                parsed++
                // cache.push(mapValues(record));
                record._IDJOB = id
                cache.push(record);
                record = parser.read()
            }
        })


        parser.on('error', function (err) {
            console.log('PARSER ERR', err);
            postMessage({id, type: 'STATS', 'args': ['errors.parsed', 1]})
            //fail.call(self, err)
        })

        parser.on('finish', function () {

            postMessage({id, type: 'STATS', 'args': ['parseCompletedAt', new Date().getTime()]})
            postMessage({id, type: 'STATS', 'args': ['status', jobStats.STATUS.PARSE_COMPLETED]})
            cache.flush()
            console.log('PARSE FINISHED')
            console.timeEnd(uid)

        })

        readStream = fs.createReadStream(file)

        readStream.on('error', function (err) {
            console.log('READER ERR', err);
            fail.call(self, err)
        })

        readStream.on('data', function (data) {
            bytes = data.length
            postMessage({id, type: 'STATS', 'args': ['bytesRead', bytes]})
        })
        readStream.pipe(parser)
        postMessage({id, type: 'STATS', 'args': ['status', jobStats.STATUS.PENDING]})


    }

    function importCSV(path) {

        const tpl = `--host=127.0.0.1 -d letsverify -c leads --type csv --file "${path}" --headerline`
        const args = tpl.split(' ');
        const child = spawn('mongoimport', args)
        child.stdout.on('data', function (data) {
            console.log('stdout: ' + data);
            //Here is where the output goes
        });
        child.stderr.on('data', function (data) {

            console.log('stderr: ' + data);
            //Here is where the error output goes
        });
        child.on('close', function (code) {
            console.log('closing code: ' + code);
            this.finishTask()
            //Here you can get the exit code of the script
        });
    }

    //keep number of running tasks
    this.tasks = 0;

    //report idle or busy status wo workerprovider
    this.finishTask = function () {
        this.tasks--
        !this.tasks && fastInsert.db.close()
        postMessage({type: this.tasks ? 'STATE_BUSY' : 'STATE_IDLE'})
    }

    this.startTask = function () {
        this.tasks++
        postMessage({type: 'STATE_BUSY'})
    }

    //start as idle
    postMessage({type: 'STATE_IDLE'})

    self.onmessage = function (event) {
        // postMessage('Hi ' + event.data);
        // self.close();

        console.log('WORKER MSG', event);
        const {data} = event
        switch (data.type) {
            case 'CLOSE_REQUEST':
                fastInsert.db.close()
                self.close()
                break;
            case 'FILE_UPLOADED':
                const {path, size, hash, name} = data.file

                if (data.mode === 'SPAWN') {
                    this.startTask()
                    importCSV.call(this, path)
                } else {
                    this.startTask()
                    bulkInsert.call(this, path, data.idjob)
                }
                break;
            default:
                postMessage({type: 'UNKNOWN_MESSAGE'})
                break;
        }


    };
}
