const _ = require('lodash')
const cache = {}

console.log('STATS PID %d', process.pid);


const log = (function throttledLog(millis) {
    let lastTime = new Date().getTime()
    return function () {
        const currTime = new Date().getTime()
        if (currTime - lastTime > millis) {
            console.log.apply(console, arguments)
            lastTime = currTime
        }
    }
})(10000)


function defaults() {
    return {
        status: 'PENDING',
        emailField:null,
        startedAt: new Date().getTime(),
        uploadStartedAt: null,
        parseCompletedAt: null,
        completedAt: null,
        erroredAt: null,
        recordCount: 0,
        parsed: 0,
        inserted: 0,
        bytesRead: 0,
        bytesTotal: 0,
        errors: {
            log: [],
            parsed: 0,
            inserted: 0,
        }
    }
}

function Stats(jobID, opts = {}) {
    if (cache[jobID]) {
        return cache[jobID]
    }
    let self = this

    //instantiate if not called with 'new'
    if (!(this instanceof Stats)) {
        self = new Stats(jobID)
    }
    self.id = jobID
    self.s = _.merge({}, defaults(), opts)

    cache[jobID] = self

    return self
}

Stats.prototype.set = function (field, val) {

    if (field === 'errors.log') {
        this.s.errors.log.push(val)
        return
    }

    const fields = field.split('.')
    let f
    let target
    if (fields[0] === 'errors') {
        target = this.s.errors
        f = fields[1]
    } else {
        target = this.s
        f = fields[0]
    }

    if ([
            'parsed',
            'inserted',
            'bytesRead',
        ].includes(f)
    ) {
        target[f] = (target[f] || 0) + val
    } else {
        target[f] = val
    }

    log('STATS #%s', this.id, this.s);

}

Stats.prototype.get = function () {
    return this.s
}

Stats.prototype.log = function () {
    console.log('STATS #%s', this.id, this.s);
}


const STATUS = {
    'PENDING': 'PENDING',
    'COMPLETED': 'COMPLETED',
    'PARSE_COMPLETED': 'PARSE_COMPLETED',
    'ERROR': 'ERROR',
}

function getAll() {
    return cache
}

function remove(id) {
    delete cache[id]
}

module.exports = {
    STATUS,
    Stats,
    getAll,
    remove
}
