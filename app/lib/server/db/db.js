const Promise = require('bluebird')
const mongoose = require('mongoose');
const CONFIG = require('../config');

const MongoClient = require('mongodb').MongoClient
const connectP = Promise.promisify(MongoClient.connect)

let _db
function connect() {
    if (_db) {
        return Promise.resolve(_db)
    }
    return connectP(CONFIG.DB.URL).then(db=> {
        _db = db
        return _db
    })
}

function close() {
    try {

        _db && _db.close()
        _db = null
    }
    catch (e) {
    }
}

module.exports = {
    connect,
    close,
}
