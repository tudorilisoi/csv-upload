//reference http://mongodb.github.io/node-mongodb-native/2.2/quick-start/quick-start/

/**
 *  this uses plain mongo as opposed to mongoose
 *  mongoose is still useful for high-level tasks
 */

const db = require('./db')
const {connect} = db


function insertDocuments(collectionName, docs, callback) {

    connect().then(
        db=> {

            // Get the documents collection
            var collection = db.collection(collectionName);
            // Insert some documents
            collection.insertMany(docs, function (err, result) {

                if (err) {
                    callback(new Error(err.code + '::' + err.errmsg), null)
                    return
                }
                // assert.equal(err, null);
                // assert.equal(3, result.result.n);
                // assert.equal(3, result.ops.length);
                // console.log("Inserted 3 documents into the collection");
                callback(null, {
                    n: result.n,
                    opsCount: result.ops.length,
                });
            });
        }
    ).catch(e=> {
        callback(e, null)
    })


}

module.exports = {
    insertDocuments,
    db,
}
