const dblib = require('./db')
const uuid = require('uuid');
const stringify = require('csv-stringify');

function download(idjob) {

    const reqid = 'DOWN_' + uuid() + ' :: ' + idjob

    const dbP = dblib.connect()

    dbP.then(db=> {

        var writable = stringify({
            header: true,
            delimiter: ',',
            rowDelimiter: 'mac',
        })

        var cursor = db.collection('leads')
            .find({_IDJOB: idjob}, {_id: 0, _IDJOB: 0})
            .stream();

        writable.on('finish', ()=> {
            // console.timeEnd(reqid)
            // res.end()
            process.exit()
        })
        cursor.pipe(writable)
        writable.pipe(process.stdout)

    })
}

module.exports = download


if (process.argv) {
    download(+process.argv[2])
}
