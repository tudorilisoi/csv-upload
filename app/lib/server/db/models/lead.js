//Import the mongoose module
var mongoose = require('mongoose');

const fieldDefs = {
    firstName: String,
    middleName: String,
    lastName: String,
    title: String,
    companyName: String,
    mailingAddress: String,
    primaryCity: String,
    primaryState: String,
    zipCode: String,
    country: String,
    phone: String,
    webAddress: String,
    email: String,
    revenue: String,
    employees: String,
    industry: String,
    subIndustry: String,
}

const schema = new mongoose.Schema(
    Object.assign({


        // binary: Buffer,
        // living: Boolean,
        // updated: { type: Date, default: Date.now },
        // age: { type: Number, min: 18, max: 65, required: true },
        // mixed: Schema.Types.Mixed,
        // _someId: Schema.Types.ObjectId,
        // array: [],
        // ofString: [String], // You can also have an array of each of the other types too.
        // nested: { stuff: { type: String, lowercase: true, trim: true } }
    }, fieldDefs)
)

const model = mongoose.model('lead', schema)

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


/**
 * transforms field names like 'firstName' to their CSV counterparts, like 'First Name'
 *
 * NOTE: if things get complicated, just return a manual map
 *
 * @returns {Array}
 */
function mapCSV() {
    const keys = Object.keys(fieldDefs)
    const map = {}

    keys.forEach(k=> {
        const matches = k.split(/([A-Z])/) //split by uppercase
        let skipNext = false
        const parts = matches.map(m=> {
            if (/([A-Z]){1}/.test(m)) { //one uppercase letter
                skipNext = true
                return ' ' + m
            } else {
                const ret = skipNext ? m : capitalizeFirstLetter(m)
                skipNext = false
                return ret
            }
        })
        map[parts.join('')] = k
    })

    return map
}

module.exports = {
    schema,
    model,
    csvFieldMap: mapCSV(),
}
