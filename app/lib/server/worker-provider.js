const Worker = require('tiny-worker');
const _ = require('lodash')
const CONFIG = require('./config');

const pool = []

let max = CONFIG.DB.UPLOAD_MAX_WORKERS

function getIdleWorker() {
    for (let i = 0; i < pool.length; i++) {
        if (pool[i].__state.busy === false) {
            return pool[i]
        }
    }
    return null
}

function getWorker(module, init, id = null) {
    const idle = getIdleWorker()
    if (idle) {
        // console.log('IDLE #%s', idle.__id);
        return idle
    }
    if (pool.length === max) {
        console.warn('*** WORKERS: ALL BUSY ***');

        // get longest working, there are chances it will finish soon
        // since chunks are mostly constant size
        const p = pool.slice(0)
        p.sort(function (a, b) {
            const ta = a.__state.busyAt
            const tb = b.__state.busyAt
            return ta === tb ? 0 : (ta < tb ? -1 : 1)
        })

        // const times = p.map(w=>w.__state.busyAt)
        // console.log('TIMES', times);

        return p[0]

        // return getRandWorker()
    }
    else {
        console.log('CREATE WORKER #%d', pool.length + 1);
        var w = new Worker(module);
        pool.push(w)
        w.__id = id || pool.length

        //keep last time when this worker got busy
        w.__state = {
            busy: false,
            busyAt: 0,
        }
        init(w)
        const old = w.onmessage
        w.onmessage = function (ev) {

            switch (ev.data.type) {
                case 'STATE_BUSY':
                    w.__state.busy = true
                    w.__state.busyAt = new Date().getTime()
                    break;
                case 'STATE_IDLE':
                    w.__state.busy = false
                    break;
                default:
                    break;
            }

            // console.log('WRAPPED', ev.data);
            old(ev)
        }
        return w
    }
}


function destroyWorker(worker) {

    const id = worker.__id

    if (worker.__state.busy) {
        console.log('NOT REMOVING busy worker #%d', id);
        return
    }

    const idx = pool.indexOf(worker)
    if (idx > -1) {
        console.log('REMOVING worker #%d', id);
        worker.postMessage({
            type: 'CLOSE_REQUEST',

        })
        pool.splice(idx, 1)
    }
}

module.exports = {
    getWorker,
    destroyWorker,
}
