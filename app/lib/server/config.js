const isDeveloping = process.env.NODE_ENV !== 'production';
const numCPUs = require('os').cpus().length

module.exports = {

    //for logging
    DEBUG: isDeveloping,

    //for file uploads
    TMPDIR: process.env.LV_USE_OS_TMPDIR ? require('os').tmpdir() : '/mnt/hd4/tmp/app-tmp',

    DB: {
        URL: process.env.LV_DB || 'mongodb://127.0.0.1/letsverify?maxPoolSize=100',
        INSERT_CHUNK_SIZE: 200, //how many rows in a batch
        UPLOAD_MAX_WORKERS: numCPUs * 2, //how many parse-upsert workers
        // UPLOAD_MAX_WORKERS: 1, //how many parse-upsert workers
    },

    SERVER: {
        PORT: process.env.PORT ? process.env.PORT : 3000,
    }
}
