const shortid = require('shortid')
const murmurhash = require('murmurhash')
module.exports = ()=> {
    return murmurhash(shortid())
}
