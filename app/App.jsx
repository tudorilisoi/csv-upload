import React from 'react';
import styles from './App.css';
import UploadUI from './components/UploadUI';
import config from './config'
export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {test: 'foo'};
    }

    render() {

        return (
            <div className="spaced">

            <UploadUI />
            </div>
        );
    }
}
