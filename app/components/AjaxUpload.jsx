import React from 'react'
import Request from '../lib/Request'
import Dots from './Dots'
import ProgressBar from './progressbar/ProgressBar'
import {If, Then, Else} from 'react-if'
import Papa from 'papaparse'
import Form from 'muicss/lib/react/form';
import Option from 'muicss/lib/react/option';
import Select from 'muicss/lib/react/select';



const initialState = {

    progress: null,
    completed: false,
    uploading: false,
    idjob: null,
    file: null,
    fields: [],
    emailField: null,
}

let counter = 0;

export default class AjaxUpload extends React.Component {
    constructor(props) {
        super(props)


        counter++
        this.id = 'upload-' + counter

        this.state = {...initialState}

        this.request = new Request({
            uri: window.location.origin + '/api/upload',
            method: 'POST',
            contentType: null, //NOTE won't work otherwise
            onInit: ()=> {
                this.setState({uploading: true})
            },

            onSuccess: (resp)=> {
                const r = JSON.parse(resp)
                this.setState({idjob: r.idjob})
            },

            onComplete: ()=> {
                const {idjob, file} = this.state
                this.setState({
                    ...initialState,
                    // idjob,
                    // file:null,
                    // completed: true,
                    // uploading:false,
                })
            },
            onUploadProgress: e=> {
                if (e.lengthComputable) {
                    const percent = (e.loaded / e.total) * 100;
                    this.setState({progress: percent.toFixed(2)})
                }

            },
        })

    }

    renderFile() {

        const {uploading, progress, file, completed, idjob, fields, emailField} = this.state
        const jobStr = idjob ? 'job #' + idjob : ''
        const pStr = progress ? '' + progress + '%' : ''

        const bar = progress ? (<ProgressBar width={progress}/>) : (<p></p>)
        if (file) {
            return (
                <div>
                    <div><strong>{file.name} {jobStr}
                        <If condition={!uploading}>
                            <div>
                                {/*<br />*/}
                                {/*<a href="#"*/}
                                {/*onClick={()=> {*/}
                                {/*this.setState({...initialState})*/}
                                {/*}}>*/}
                                {/*Choose another file*/}
                                {/*</a>*/}

                                <Form>
                                    <Select
                                        label="Select e-mail column"
                                        defaultValue={this.state.emailField || ''}
                                        onChange={ev=> {
                                            this.setState({emailField: ev.target.value})
                                        }}
                                    >
                                        <Option key={-1} value={''} label={'--None--'}/>

                                        {fields.map((f, i)=> {
                                            return (
                                                <Option key={i} value={f} label={f}/>
                                            )
                                        })}
                                    </Select>

                                </Form>


                            </div>
                        </If>
                    </strong></div>
                    <If condition={uploading}>
                        <Then>
                            <div>
                                <p>Uploading {pStr} <Dots count="5"/></p>
                                {bar}
                                <button
                                    className="mui-btn mui-btn--danger"
                                    onClick={()=> {
                                        this.request.abort();
                                        this.setState({...initialState})
                                    }}
                                >Abort
                                </button>
                            </div>
                        </Then>
                        <Else>
                            <div className="buttons-row">
                                <button
                                    className="mui-btn mui-btn--primary"
                                    onClick={ev=> {
                                        ev.preventDefault()
                                        this.doUpload()
                                    }}
                                >Start
                                </button>

                                <button
                                    className="mui-btn mui-btn--dark"
                                    disabled={fields.length === 0}
                                    onClick={()=> {
                                        this.setState({...initialState})
                                    }}>
                                    Choose another file
                                </button>
                            </div>
                        </Else>
                    </If>
                </div>
            )
        }


        return (

            <div>
                <label htmlFor={this.id} className=" upload--label mui-btn mui-btn--primary">
                    {completed ? 'Upload another file' : 'Choose file (CSV only)'}
                    <input
                        id={this.id}
                        className="upload"
                        ref={(el)=>this.$fileInput = el}
                        disabled={uploading}
                        type="file" onChange={ev=>this.handleFileSelect(ev)}/>
                </label>
                &nbsp;
                {completed && filename ? (<p>{filename + ' completed, job #' + idjob}</p>) : null}
            </div>
        )
    }

    // componentWillMount(){}
    // componentDidMount(){}
    componentWillUnmount() {
        this.request.abort()
    }

    doUpload() {
        const {file, emailField} = this.state

        if (!file) {
            return
        }

        if (!/\.csv/.test(file.name)) {
            return
        }

        const d = new FormData()
        d.append('upload', file)

        //client time is not reliable, but hey...
        d.append('uploadStartedAt', new Date().getTime())
        d.append('emailField', emailField)

        this.setState({loading: true})

        this.request.send(d)
    }

    handleFileSelect(ev) {
        ev.persist()
        console.log('FILE EV', ev);
        const file = this.$fileInput.files[0]
        let parsed = false
        let lines = 0

        const config = {
            header: true,
            preview: 10,
            complete: data=> {
                if (!parsed) {
                    console.log('PARSED!', data);
                    parsed = true
                    this.setState({
                        ...initialState,
                        file,
                        fields: data.meta.fields || []
                    })
                }

            },
            error: (error, file)=> {

                console.log('PARSE ERR!', error);
                this.setState({
                    ...initialState,
                    file,
                    fields: []
                })

            }
        }

        this.setState({file:{name:'Parsing columns...'}},()=>Papa.parse(file, config))



    }

    render() {

        const {fields} = this.state

        return (
            <div >
                <div className="w100">
                    {this.renderFile()}
                </div>
            </div>
        )
    }
}


AjaxUpload.propTypes = {
    virtual: React.PropTypes.any, //doc here

}

AjaxUpload.defaultProps = {
    virtual: null,
}
