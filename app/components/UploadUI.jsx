import _ from 'lodash'
import React from 'react'
import ServerStatus from './ServerStatus'
import AjaxUpload from './AjaxUpload'
import JobBrowser from './JobBrowser'
import Request from '../lib/Request'
import FlipMove from 'react-flip-move'
import ReactModal from  'react-modal'
ReactModal.defaultStyles.overlay.backgroundColor = 'rgba(128,128,128,.8)';
//ReactModal.defaultStyles.overlay.opacity = '.5';
ReactModal.defaultStyles.overlay.zIndex = 9998;
// ReactModal.defaultStyles.content.maxWidth = '1270px';
ReactModal.defaultStyles.content = {
    width: '95%',
    maxWidth: '1270px',
    position: 'absolute',
    top: '40px',
    left: '50%',
    transform: 'translateX(-50%)',
    // right: '40px',
    bottom: '40px',
    // margin:'0 40px 0 40px',
    border: '1px solid #ccc',
    background: '#fff',
    overflow: 'auto',
    WebkitOverflowScrolling: 'touch',
    borderRadius: '4px',
    outline: 'none',
    padding: '20px'

}


export default class UploadUI extends React.Component {
    constructor(props) {
        super(props)

        this.handleCloseModal = this.handleCloseModal.bind(this)

        this.request = new Request({
            uri: window.location.origin + '/api/database',
            onInit: ()=> {
                this.setState({loading: true})
            },
            onComplete: ()=> {
                this.setState({loading: false})
            },
        })

        this.state = {
            showModal: false,
            dropmessage: 'Delete everything',
            clearmessage: 'Clear finished jobs',
            showcompletedjobs: true,
            uploads: [0]
        }


    }

    handleCloseModal(cleanEmailField = false) {
        this.setState({
            showModal: false,
        })
    }


    //sets a message
    toggleState(field, value, nextValue) {
        const old = nextValue !== undefined ? nextValue : this.state[field]
        const prev = {}
        prev[field] = old
        window.clearTimeout(this.messagetimer)
        const next = {}
        next[field] = value
        this.setState(next, ()=> {
            this.timers = this.timers || {}
            window.clearTimeout(this.timers[field])
            this.timers[field] = window.setTimeout(()=> {
                this.setState(prev)
            }, 5000)
        })
    }

    dropCollection() {
        if (!confirm('Drop all records?')) {
            return
        }

        this.request.sendJSON({op: 'drop'}).then(resp=> {
            this.toggleState('dropmessage', 'Deleted!', 'Delete everything')
            console.log('RESP', resp);


        }).catch(e=> {
            console.log('ERR', e);
            this.toggleState('dropmessage', 'There was an error!', 'Delete everything')
        }).finally(()=> {
            this.toggleState('loading', true, false)
        })
    }

    clearFinishedJobs() {
        if (!confirm('Clear finished jobs on the server?')) {
            return
        }

        this.request.sendJSON({op: 'clearfinished'}).then(resp=> {
            this.toggleState('clearmessage', 'Cleared!', 'Clear finished jobs')
            console.log('RESP', resp);


        }).catch(e=> {
            console.log('ERR', e);
            this.toggleState('clearmessage', 'There was an error!', 'Clear finished jobs')
        }).finally(()=> {
            this.toggleState('loading', true, false)
        })
    }


    // componentWillMount(){}
    // componentDidMount(){}
    componentWillUnmount() {
        if (this.timers) {
            _.map(this.timers, window.clearTimeout)
        }
    }

    addUpload() {
        const {uploads}  = this.state
        const next = uploads.slice(0)
        next.push(uploads.length)
        this.setState({
            uploads: next
        })
    }

    removeUpload(v) {
        const {uploads}  = this.state
        const idx = uploads.indexOf(v)
        const next = uploads.slice(0)
        next.splice(idx, 1)
        this.setState({
            uploads: next
        })
    }

    renderSlot(v) {

        let remove = null
        if (v) {
            remove = (
                <a className="mui--text-danger"
                   href="#"
                   onClick={ev=> {
                       this.removeUpload(v)
                   }}
                >
                    X
                </a>
            )
        }

        return (
            <div key={'upload-row-' + v} className="mui-panel">

                <div className="mui-col-md-10">
                    <AjaxUpload />
                </div>

                <div className="mui-col-md-2 mui--text-right">
                    {remove}
                </div>

            </div>
        )
    }

    render() {

        const {showcompletedjobs} = this.state

        return (
            <div className="mui-container">
                <div className="v-spaced"></div>
                <div className="mui-row">
                    <div className="mui-col-md-6">
                        <div className="xmui-panel">
                            <h3 className="mui--align-baseline">Upload

                            </h3>
                            <FlipMove
                                enterAnimation="accordionVertical"
                                leaveAnimation="accordionVertical"
                            >
                                {this.state.uploads.map(v=>this.renderSlot(v))}

                                <div>
                                    <a className=" mui--text-subhead" href="#"
                                       onClick={ev=>this.addUpload(ev)}>
                                        Add upload slot
                                    </a>
                                </div>

                            </FlipMove>
                        </div>
                    </div>
                    <div className="mui-col-md-6">
                        <div className="xmui-panel">

                            <h3>Server ops</h3>
                            <div className="mui-panel mui--clearfix">
                                <div className="buttons-row">

                                    <button
                                        disabled={this.state.loading}
                                        onClick={ev=>this.dropCollection()}
                                        className="mui-btn mui-btn--small mui-btn--danger">
                                        {this.state.dropmessage}
                                    </button>
                                    <button
                                        className="mui-btn mui-btn--small mui-btn--primary"
                                        onClick={()=>this.setState({showModal: true})}
                                    >
                                        Open statistics
                                    </button>

                                    {/*<button*/}
                                    {/*disabled={this.state.loading}*/}
                                    {/*onClick={ev=>this.clearFinishedJobs()}*/}
                                    {/*className="mui-btn mui-btn--small mui-btn--danger mui--pull-right">*/}
                                    {/*{this.state.clearmessage}*/}
                                    {/*</button>*/}

                                </div>
                            </div>
                            <h3 className="mui--clearfix">
                                Processing queue
                                <a className="mui--pull-right" href="#"
                                   onClick={ev=> {
                                       this.setState({showcompletedjobs: !showcompletedjobs})
                                   }}>
                                    <small>{showcompletedjobs ? 'Hide completed (older than 10s)' : 'Show completed'}</small>
                                </a>
                            </h3>
                            <ServerStatus showCompleted={showcompletedjobs}/>
                        </div>
                    </div>
                </div>
                <ReactModal
                    isOpen={this.state.showModal}
                    contentLabel="Job stats"
                    onRequestClose={this.handleCloseModal}
                >

                    <JobBrowser handleCloseModal={this.handleCloseModal}/>
                </ReactModal>
            </div>
        )
    }
}


UploadUI.propTypes = {
    virtual: React.PropTypes.any, //doc here

}

UploadUI.defaultProps = {
    virtual: null,
}
