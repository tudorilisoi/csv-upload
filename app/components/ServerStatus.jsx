import _ from 'lodash'
import moment from 'moment'
import momentDuration from 'moment-duration-format'
import React from 'react'
import Request from '../lib/Request'
import Dots from './Dots'
import ProgressBar from './progressbar/ProgressBar'
import _css from './ServerStatus.css'

import FlipMove from 'react-flip-move'


export default class ServerStatus extends React.Component {
    constructor(props) {
        super(props)
        this.request = new Request({
            uri: window.location.origin + '/api/upload/status',
        })

        this.state = {
            status: {},
            message: null,
        }

    }

    componentDidMount() {

        const poll = ()=> {

            this.request.sendJSON({t: new Date().getTime()}).then(resp=> {
                // console.log('RESP', resp);
                this.setState({status: resp, message: null},)
            }).catch(err=> {
                // console.log('ERR', err);
                this.setState({message: 'Connection error'})
            }).finally(()=> {
                this.timer = window.setTimeout(poll, 3000)
            })
        }
        poll()
    }

    componentWillUnmount() {
        this.request.abort()
        window.clearTimeout(this.timer)
    }

    // componentWillMount(){}

    renderJob(stats, serverTime) {

        const {id, s} = stats
        const _s = JSON.stringify(s, 2, 2)

        const {
            filename,
            status,
            errors,
            startedAt,
            completedAt,
            erroredAt,
            parseCompletedAt,
            recordCount,
            parsed,
            inserted,
            bytesRead,
            bytesTotal
        } = s

        let task, curr, total

        if (recordCount) {
            task = 'Storing'
            curr = inserted
            total = recordCount

        } else {
            task = 'Parsing'
            curr = bytesRead
            total = bytesTotal
        }

        let dots = null, records = inserted, pbarclass = ''
        if (errors.log.length > 0) {
            task = 'Server error'
            pbarclass = 'progress-bar--error'
        } else if (status === 'COMPLETED') {
            task = 'Success!'
            pbarclass = 'progress-bar--success'

        } else if (status === 'PARSE_COMPLETED') {
            pbarclass = 'progress-bar--finishing'
            dots = (<strong><Dots count="6" delay={160}/></strong>)

        } else {
            // pbarclass = 'progress-bar--success'
            dots = (<strong><Dots count="3" delay={500}/></strong>)
        }


        const percent = !total ? 0 : (curr / total * 100).toFixed(2)


        const seconds = moment(erroredAt || completedAt || serverTime).diff(moment(startedAt), 'seconds')
        const runningTime = moment.duration(seconds, 's').format('mm:ss', {trim: false})


        return (
            <div key={id} className="mui-panel">
                <table width="100%">
                    <tbody>
                    <tr>
                        <td>
                            Job #
                        </td>
                        <td className="mui--text-right">
                            {id}
                        </td>
                    </tr>
                    <tr>
                        <td colSpan="2">
                            <strong>{filename}</strong>
                        </td>

                    </tr>

                    <tr>
                        <td colSpan="2">

                            <ProgressBar width={percent} innerClassName={pbarclass}/>
                        </td>

                    </tr>


                    <tr>
                        <td>
                            Running time: {dots}
                        </td>
                        <td className="mui--text-right">
                            {runningTime}
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Status: <strong>{task}</strong>
                        </td>
                        <td className="mui--text-right">
                            {percent}%
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Records:
                        </td>
                        <td className="mui--text-right">
                            <strong> {records}</strong>
                        </td>
                    </tr>

                    </tbody>
                </table>
                <pre>
                    {/*{_s}*/}
                </pre>
            </div>
        )
    }

    renderStats(s) {

        const {status} = this.state
        // console.log('STATUS', status);
        const {stats, now}  = status //now=server time
        const ids = Object.keys(stats || {})
        if (!ids.length) {
            return (<p>No jobs</p>)
        }

        const filteredids = ids.filter(id=> {
            const s = stats[id].s
            if (s.status === 'COMPLETED' && !this.props.showCompleted) {
                const seconds = moment(now).diff(moment(s.completedAt), 'seconds')
                if (seconds > 10) {
                    return false
                }
            }
            return true
        })
        //sort recents first
        filteredids.sort(function (a, b) {

            const ta = stats[a].s.startedAt
            const tb = stats[b].s.startedAt
            return ta === tb ? 0 : (ta < tb ? 1 : -1)
        })

        const jobs = filteredids.map(id=> {
            return this.renderJob(stats[id], now)
        })

        return (
            <div>
                <FlipMove
                    enterAnimation="accordionVertical"
                    leaveAnimation="accordionVertical"
                >
                    {jobs}
                </FlipMove>
            </div>
        )

    }

    render() {
        return (
            <div>
                {this.renderStats()}
            </div>
        )
    }
}


ServerStatus.propTypes = {
    showCompleted: React.PropTypes.bool, //doc here

}

ServerStatus.defaultProps = {
    showCompleted: true,
}
