// import _ from 'lodash'
import moment from 'moment'
import React from 'react'
import Reactable from 'reactable'
const Table = Reactable.Table,
    unsafe = Reactable.unsafe,
    Tr = Reactable.Tr,
    Td = Reactable.Td;

import Request from '../lib/Request'
import _css from './JobBrowser.css'

export default class JobBrowser extends React.Component {
    constructor(props) {
        super(props)

        this.request = new Request({
            uri: window.location.origin + '/api/stats',
            onInit: ()=> {
                this.setState({loading: true})
            },

            onComplete: ()=> {
                this.setState({loading: false})
            },
        })

        this.state = {
            loading: false,
            offset: 0,
            limit: 100,
            rows: [],
        }
    }

    sendRequest() {
        const {offset, limit} = this.state
        this.setState({rows: []}, ()=> {
            this.request.sendJSON(
                {offset, limit},
                {
                    onSuccess: result=> {
                        // debugger
                        this.setState({rows: result.rows || []})
                    },
                }
            )
        })
    }


    // componentWillMount(){}
    componentDidMount() {
        this.sendRequest()
    }

    // componentWillUnmount(){}


    handleDelete(ev, idjob) {
        ev.preventDefault()
        if (!confirm('Really delete?')) {
            return
        }

        this.request.sendJSON(
            {op: 'deletejob', idjob,},
            {
                uri: window.location.origin + '/api/database',
                onSuccess: result=> {
                    // debugger
                    this.sendRequest()
                },
                onError: err=> {
                    // debugger
                    alert('Server error, try again later')
                },
            }
        )

    }

    render() {

        const baseURI = window.location.origin + '/api/download/'

        const {rows, loading, offset, limit} = this.state

        const _rows = rows.map(r=> {

                const {
                    uploadStartedAt,
                    emailField,
                    filename,
                    status,
                    errors,
                    startedAt,
                    completedAt,
                    erroredAt,
                    parseCompletedAt,
                    recordCount,
                    parsed,
                    inserted,
                    bytesRead,
                    bytesTotal
                } = r

                const seconds = moment(erroredAt || completedAt || serverTime).diff(moment(startedAt), 'seconds')
                const runningTime = moment.duration(seconds, 's').format('mm:ss', {trim: false})
                const startTime = moment(+uploadStartedAt).format('MM/DD/YY HH:mm:ss')


                const href = baseURI + r.id

                return {
                    'Filename': filename,
                    'Started at': startTime,
                    // 'ID': r.id,
                    'Duration': runningTime,
                    'Status': r.status === 'COMPLETED' ? 'OK' : r.errors.log[0],
                    'E-mail field': emailField !== 'null' ? emailField : '-',
                    'Actions': (

                        <div>
                            <a href="#"
                               onClick={ev=>this.handleDelete(ev, r.id)}
                            >Delete</a>
                            &nbsp; &nbsp;
                            <a target="_blank"
                               href={href}>
                                Download
                            </a>
                        </div>
                    )
                }

            }
        )

        let sortConfig = []
        if (_rows[0]) {
            sortConfig = Object.keys(_rows[0])
                .filter(i=>!(['Actions'].includes(i)))
        }

        return (
            <div className="viewport flex-columns">
                <div className="flex flex-no-shrink">
                    <h3 className="w100">
                        Stats
                        &nbsp; &nbsp;
                        <a
                            className="mui--pull-right"
                            href="#" onClick={()=>this.sendRequest()}>Refresh</a>
                    </h3>
                </div>
                <div className="flex flex-grow scroll-y">

                    <Table
                        className="mui-table mui-table--bordered jobs--table"
                        id="jobs--table"
                        width="100%"
                        sortable={sortConfig}
                        defaultSort={{column: 'Started at', direction: 'desc'}}
                        data={_rows}
                        noDataText="No matching records found."
                        >

                    </Table>
                </div>
                <div className="flex flex-no-shrink">
                    <div>
                        <button
                            onClick={this.props.handleCloseModal}
                            className="mui-btn mui-btn--raised">Close
                        </button>
                    </div>
                </div>

            </div>

        )
    }
}


JobBrowser.propTypes = {
    // jobs: React.PropTypes.Array, //doc here

}

JobBrowser.defaultProps = {
    // jobs: [],
}
