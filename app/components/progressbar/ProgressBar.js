import React, {Component} from 'react';
// eslint-disable-next-line
import _css from './ProgressBar.css'


class ProgressBar extends Component {
    render() {

        const {width, innerClassName, ...otherProps} =this.props
        const style = {
            width:''+width+'%'
        }

        return (
            <div className="progressbar" {...otherProps}>
                <div className={'progressbar-inner ' + innerClassName} style={style}>
                </div>
            </div>
        )
    }
}

ProgressBar.defaultProps = {
    innerClassName: '',
}

export default ProgressBar
