var path = require('path')

module.exports = {
    nodeModules: path.join(__dirname, 'node_modules'),
    src: path.join(__dirname, 'app'),
    root: path.join(__dirname, 'dist'),
};


